# KoalaWM
`KoalaWM` is a brand spanking new Window manager. It's availble for any distro that uses Xorg. Sorry folks, it does not use Wayland.

It's _Minimal_, around __~400MB RAM__ usage on Linux Mint 20.

But, these are the only __2 cons of it__:
- No Window Titlebars, or resizing
- __EXTREMELY buggy__.

Also, it's easy to configure. We hope you enjoy it!