# KoalaWM

`KoalaWM` is a very new window manager for Linux. It is __VERY BUGGY__ at this stage.

To compile, run:
```
chmod +x install
sudo ./install
```

Configuring is easy! Just replace the variables, and if you want, add extra tasks by just typing the command.

What the config file would look like when you added to it, __just ignore the junk lines__:
```
(Junk)
myKeybindManager="xbindkeys --key"
# Now, run the apps
$myDesktopManager &
$MyKeybindManager &
(Junk)
surf &
hang
```
Note that the config is __NOT__ in your home dir. It's located at `/etc/eleaves`. __It's a file, not a dir__. 

What's interesting about it is the fact that, the __configuration file, is the source code__. Without it, You can't use KoalaWM.